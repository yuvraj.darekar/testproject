package com.example.testproject.api;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.Named;


@Api(name = "helloapi", version = "v1")
public class MyApi {
	@ApiMethod(name = "sayHello", path = "hello/{name}", httpMethod = ApiMethod.HttpMethod.GET)
	public String sayHello(@Named("name") String name) {
		return "Hello " + name + "!";
//		HelloResponse response = new HelloResponse();
//		response.setMessage("Hello, " + name + "!");
//		return response;
	}
}
